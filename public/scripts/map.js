function initMap() {
    window.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: {lat: 24.886, lng: -70.268},
        mapTypeId: 'terrain'
    });

    var input = document.getElementById('search');
    window.searchBox = new google.maps.places.SearchBox(input);
    window.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    window.map.addListener('bounds_changed', function() {
        window.searchBox.setBounds(window.map.getBounds());
    });

    window.searchBox.addListener('places_changed', function() {
        var places = window.searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        if(!window.markers) {
            window.markers = [];
        }

        window.markers.forEach(function(marker) {
            marker.setMap(null);
        });

        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                return;
            }
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            window.markers.push(new google.maps.Marker({
                map: window.map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        window.map.fitBounds(bounds);
    });
}

function addPin() {
    if(!window.pins) {
        window.pins = [];
    }

    var marker = new google.maps.Marker({
        map: window.map,
        title: "Pin",
        position: { lat: parseFloat(document.getElementById('lat').value) , lng: parseFloat(document.getElementById('lon').value) }
    });
    window.pins.push(marker);
}

function addVertices() {
    var verticesValue = document.getElementById('vertices-list').value;
    var cords = [];

    var vertices = verticesValue.split(',');

    for(var i in vertices) {
        var vertice = vertices[i].trim().split(' ');

        cords.push({
            lat: parseFloat(vertice[1].trim()),
            lng: parseFloat(vertice[0].trim())
        });
    }

    var polygon = new google.maps.Polygon({
        paths: cords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.4,
        strokeWeight: 1,
        fillColor: '#FF0000',
        fillOpacity: 0.25
    });
    polygon.setMap(window.map);
}
